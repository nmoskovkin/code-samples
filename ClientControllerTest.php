<?php
namespace App\Tests\Controller;

use App\Controller\ClientController;
use App\Entity\Company;
use App\Entity\Department;
use App\Entity\Industry;
use App\Entity\User;
use App\Service\RecaptchaService;
use App\Tests\_helper\BaseWebTestCase;
use App\Tests\_helper\FixtureFactory;
use App\Validator\Constraint\ClientPublicRegistrationValidator;

class ClientControllerTest extends BaseWebTestCase
{
    /**
     * @test
     */
    public function PostClientRegister_EmptyRequest_Returns400()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/clients/public-registration',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{}'
        );
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    /**
     * @test
     * @dataProvider putClientDataProvider
     */
    public function PutClient_ValidAndInvalidRequests_ValidationWorking(
        \Closure $modificator,
        $expectedStatus,
        $expectedPropertyPath
    ) {
        $client = static::createClient();
        $company = $this->getFixtureFactory()->get(Company::class);
        $department = $this->getFixtureFactory()->get(Department::class, [
            'company' => $company
        ]);
        $companyClient = $this->getFixtureFactory()->get(User::class, [
            'isClient' => 1,
            'roles' => [User::ROLE_CLIENT],
            'company' => $company,
            'department' => $department,
        ]);
        $this->getEm()->flush();
        $this->loginUser($client, $companyClient);

        $data = [
            'firstName' => 'First',
            'lastName' => 'Last',
            'username' => 'username111',
            'email' => 'username_111@gmail.com',
            'departmentId' => $department->getId(),
        ];
        $data = $modificator($data);

        $client->request(
            'PUT',
            sprintf('/api/clients/%d', $companyClient->getId()),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );

        $this->assertEquals($expectedStatus, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent());
        if ($expectedStatus === 400) {
            $this->assertEquals($expectedPropertyPath, $response->errors[0]->property_path);
        }
    }

    /**
     * @test
     */
    public function GetClients_FilterByCompanyName_ReturnsCorrectListOfClients()
    {
        $client = static::createClient();
        $company = $this->getFixtureFactory()->get(Company::class, ['name' => '123_test_123']);

        $this->getFixtureFactory()->get(
            User::class,
            [
                'isClient' => 1,
                'company' => $company,
                'department' => null,
                'roles' => ['ROLE_CLIENT'],
            ]
        );
        $this->getFixtureFactory()->get(
            User::class,
            [
                'isClient' => 1,
                'company' => $company,
                'department' => null,
                'roles' => ['ROLE_CLIENT'],
            ]
        );

        // Another company
        $this->getFixtureFactory()->get(
            User::class,
            [
                'isClient' => 1,
                'department' => null,
                'roles' => ['ROLE_CLIENT'],
            ]
        );

        $admin = $this->getFixtureFactory()->get(
            User::class,
            [
                'department' => null,
                'roles' => ['ROLE_ADMIN'],
            ]
        );

        $this->getEm()->flush();
        $this->loginUser($client, $admin);
        $this->getEm()->clear();

        $client->request('GET', sprintf('/api/clients?company_name[like]=%s', 'test'));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent());

        $this->assertCount(2, $response->data);
    }

    /**
     * @dataProvider filterDataProvider
     * @test
     */
    public function GetClients_Filtering_ReturnsCorrectResult($url)
    {
        $client = static::createClient();
        $company = $this->getFixtureFactory()->get(Company::class);

        $this->getFixtureFactory()->get(
            User::class,
            [
                'isClient' => 1,
                'company' => $company,
                'department' => null,
                'roles' => ['ROLE_CLIENT'],
                'firstName' => '321_an_123',
                'lastName' => 'a_abc_a',
                'email' => 'test123@example.com'
            ]
        );
        $this->getFixtureFactory()->get(
            User::class,
            [
                'isClient' => 1,
                'company' => $company,
                'department' => null,
                'roles' => ['ROLE_CLIENT'],
                'firstName' => '321_b_123',
                'lastName' => 'a_abd_a',
                'email' => 'test124@example.com'
            ]
        );
        $this->getFixtureFactory()->get(
            User::class,
            [
                'isClient' => 1,
                'company' => $company,
                'department' => null,
                'roles' => ['ROLE_CLIENT'],
                'firstName' => '321_c_123',
                'lastName' => 'a_abe_a',
                'email' => 'test125@example.com'
            ]
        );

        $admin = $this->getFixtureFactory()->get(
            User::class,
            [
                'department' => null,
                'roles' => ['ROLE_ADMIN'],
            ]
        );

        $this->getEm()->flush();
        $this->loginUser($client, $admin);
        $this->getEm()->clear();

        $client->request('GET', $url);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent());

        $this->assertCount(1, $response->data);
    }

    public function filterDataProvider()
    {
        return [
            [
                '/api/clients?first_name[like]=an'
            ],
            [
                '/api/clients?last_name[like]=abc'
            ],
            [
                '/api/clients?email[like]=123'
            ]
        ];
    }

    /**
     * @test
     */
    public function GetClients_FilterByIndustry_ReturnsCorrectListOfClients()
    {
        $client = static::createClient();
        $industry1 = $this->getFixtureFactory()->get(Industry::class);
        $industry2 = $this->getFixtureFactory()->get(Industry::class);

        $company1 = $this->getFixtureFactory()->get(Company::class, ['industries' => [$industry1]]);
        $company2 = $this->getFixtureFactory()->get(Company::class, ['industries' => [$industry2]]);

        $this->getFixtureFactory()->get(
            User::class,
            [
                'isClient' => 1,
                'company' => $company1,
                'department' => null,
                'roles' => ['ROLE_CLIENT'],
            ]
        );
        $this->getFixtureFactory()->get(
            User::class,
            [
                'isClient' => 1,
                'company' => $company2,
                'department' => null,
                'roles' => ['ROLE_CLIENT'],
            ]
        );
        $this->getFixtureFactory()->get(
            User::class,
            [
                'isClient' => 1,
                'company' => $company2,
                'department' => null,
                'roles' => ['ROLE_CLIENT'],
            ]
        );

        $admin = $this->getFixtureFactory()->get(
            User::class,
            [
                'isStudent' => 1,
                'department' => null,
                'roles' => ['ROLE_ADMIN'],
            ]
        );

        $this->getEm()->flush();
        $this->loginUser($client, $admin);
        $this->getEm()->clear();

        $client->request('GET', sprintf('/api/clients?industry_ids=%d', $industry1->getId()));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent());
        $this->assertCount(1, $response->data);

        $client->request('GET', sprintf('/api/clients?industry_ids=%d', $industry2->getId()));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent());
        $this->assertCount(2, $response->data);

        $client->request('GET', sprintf(
            '/api/clients?industry_ids=%d,%d',
            $industry2->getId(),
            $industry1->getId()
        ));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent());
        $this->assertCount(3, $response->data);
    }

    /**
     * @test
     */
    public function PostPublicRegistration_ValidData_CreatesNewClientAndSendsALinkOnEmail()
    {
        $client = static::createClient();

        $data = [
            'email' => 'test@example.com',
            'recaptcha' => 'something',
            'first_name' => 'First',
            'last_name' => 'Last name',
            'privacyPolicy' => 1,
        ];

        $recaptchaServiceMock = $this
            ->getMockBuilder(RecaptchaService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $recaptchaServiceMock->expects($this->once())->method('verify')->willReturn(true);
        $this->replaceServiceDependency(ClientPublicRegistrationValidator::class, 'recaptchaService', $recaptchaServiceMock);

        $client->request(
            'POST',
            '/api/clients/public-registration',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->getEm()->clear();

        /** @var User[] $users */
        $users = $this->getEm()->getRepository(User::class)->findAll();
        $this->assertCount(1, $users);
        $this->assertNotEmpty($users[0]->getActivationCode());
        $this->assertEquals(User::HASH_TYPE_CLIENT_PUBLIC_REGISTRATION, $users[0]->getActivationCodeType());
    }

    /**
     * @test
     *
     * @dataProvider publicRegistrationDataProvider
     */
    public function PostPublicRegistration_TestMessages_ReturnsCorrectResponse(
        \Closure $modificator,
        int $statusCode,
        ?string $propertyPath
    ) {
        $client = static::createClient();

        $data = [
            'email' => 'test@example.com',
            'recaptcha' => 'something',
            'first_name' => 'First',
            'last_name' => 'Last name',
            'privacyPolicy' => 1,
        ];

        $recaptchaServiceMock = $this
            ->getMockBuilder(RecaptchaService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $recaptchaServiceMock->method('verify')->willReturn(true);
        $this->replaceServiceDependency(ClientPublicRegistrationValidator::class, 'recaptchaService', $recaptchaServiceMock);

        $data = $modificator($data);
        $client->request(
            'POST',
            '/api/clients/public-registration',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );

        $this->assertEquals($statusCode, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent());

        if ($propertyPath) {
            $this->assertEquals($propertyPath, $response->errors[0]->property_path);
        }
    }

    public function publicRegistrationDataProvider()
    {
        return [
            [
                function($data) {
                    unset($data['recaptcha']);

                    return $data;
                },
                400,
                'recaptcha'
            ],
            [
                function($data) {
                    $data['recaptcha'] = null;

                    return $data;
                },
                400,
                'recaptcha'
            ],
            [
                function($data) {
                    $data['recaptcha'] = '';

                    return $data;
                },
                400,
                'recaptcha'
            ],
            [
                function($data) {
                    unset($data['first_name']);

                    return $data;
                },
                400,
                'first_name'
            ],
            [
                function($data) {
                    $data['first_name'] = null;

                    return $data;
                },
                400,
                'first_name'
            ],
            [
                function($data) {
                    unset($data['last_name']);

                    return $data;
                },
                400,
                'last_name'
            ],
            [
                function($data) {
                    $data['last_name'] = null;

                    return $data;
                },
                400,
                'last_name'
            ],
            [
                function($data) {
                    unset($data['email']);

                    return $data;
                },
                400,
                'email'
            ],
            [
                function($data) {
                    $data['email'] = null;

                    return $data;
                },
                400,
                'email'
            ],
            [
                function($data) {
                    $data['email'] = 'test@test@test';

                    return $data;
                },
                400,
                'email'
            ],
            [
                function($data) {
                    unset($data['privacyPolicy']);

                    return $data;
                },
                400,
                'privacy_policy'
            ],
            [
                function($data) {
                    $data['privacyPolicy'] = 0;

                    return $data;
                },
                400,
                'privacy_policy'
            ],
            [
                function($data) {
                    $data['privacyPolicy'] = false;

                    return $data;
                },
                400,
                'privacy_policy'
            ],
            [
                function($data) {
                    $data['privacyPolicy'] = null;

                    return $data;
                },
                400,
                'privacy_policy'
            ],
        ];
    }

    public function putClientDataProvider()
    {
        return [
            [
                function ($data) {
                    $data['first_name'] = null;

                    return $data;
                },
                400,
                'first_name',
            ],
            [
                function ($data) {
                    $data['last_name'] = null;

                    return $data;
                },
                400,
                'last_name'
            ],
            [
                function ($data) {
                    $data['email'] = null;

                    return $data;
                },
                400,
                'email'
            ],
            [
                function ($data) {
                    $data['username'] = null;

                    return $data;
                },
                400,
                'username'
            ],
            [
                function ($data) {
                    $data['department_id'] = null;

                    return $data;
                },
                200,
                ''
            ],
        ];
    }

    /**
     * @test
     */
    public function PatchClientActivation_ValidData_InsertRows()
    {
        $client = static::createClient();

        $companyClient = $this->getFixtureFactory()->get(User::class, [
            'isClient' => 1,
            'activationCode' => 'a1b2c3d5',
            'activationCodeType' => User::HASH_TYPE_CLIENT_PUBLIC_REGISTRATION,
            'company' => null
        ]);

        $data = [
            'username' => 'test@example.com',
            'password' => 'password',
            'confirm_password' => 'password',
            'company_name' => 'Test Company',
            'phone_number' => '235-34-35',
        ];

        $client->request(
            'PATCH',
            '/api/client-by-hash/a1b2c3d5/activate',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->getEm()->clear();

        $companyClient = $this->getEm()->getRepository(User::class)->find($companyClient->getId());
        $this->assertNotEmpty($companyClient->getCompany());
        $this->assertEquals($companyClient->getCompany()->getName(), 'Test Company');
        $this->assertEmpty($companyClient->getActivationCode());
        $this->assertEmpty($companyClient->getActivationCodeType());


        $client->request(
            'POST',
            '/api/login',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'username' => 'test@example.com',
                'password' => 'password'
            ])
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function PatchClientActivation_UrlIsNotCorrect_Returns404()
    {
        $client = static::createClient();

        $companyClient = $this->getFixtureFactory()->get(User::class, [
            'isClient' => 1,
            'activationCode' => 'a1b2c3d5',
            'activationCodeType' => User::HASH_TYPE_CLIENT_PUBLIC_REGISTRATION,
            'company' => null
        ]);

        $data = [
            'username' => 'test@example.com',
            'password' => 'password',
            'confirm_password' => 'password',
            'company_name' => 'Test Company',
            'phone_number' => '235-34-35',
        ];

        $client->request(
            'PATCH',
            '/api/client-by-hash/asdfasdf/activate',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    /**
     * @test
     *
     * @dataProvider patchClientActivationProvider
     */
    public function PatchClientActivation_InvalidData_Returs400HttpCode(
        \Closure $requestModificator,
        int $statusCode,
        ?string $propertyPath,
        ?\Closure $clientDataModificator = null
    ) {
        $client = static::createClient();

        $clientData = [
            'isClient' => 1,
            'activationCode' => 'a1b2c3d5',
            'activationCodeType' => User::HASH_TYPE_CLIENT_PUBLIC_REGISTRATION,
            'company' => null
        ];
        if ($clientDataModificator) {
            $clientData = $clientDataModificator($clientData, $this->getFixtureFactory());
        }

        $this->getFixtureFactory()->get(User::class, $clientData);

        $requestData = [
            'username' => 'test@example.com',
            'password' => 'password',
            'confirm_password' => 'password',
            'company_name' => 'Test Company',
            'phone_number' => '235-34-35',
        ];

        $data = $requestModificator($requestData);

        $client->request(
            'PATCH',
            '/api/client-by-hash/a1b2c3d5/activate',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );

        $this->assertEquals($statusCode, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent());
        if ($propertyPath) {
            $this->assertEquals($propertyPath, $response->errors[0]->property_path);
        }
    }

    public function patchClientActivationProvider()
    {
        return [
            [
                function($data) {
                    return $data;
                },
                400,
                'company_name',
                function($clientData, FixtureFactory $fixtureFactory) {
                    $clientData['company'] = $fixtureFactory->get(Company::class);

                    return $clientData;
                }
            ],
            [
                function($data) {
                    return $data;
                },
                404,
                '',
                function($clientData, FixtureFactory $fixtureFactory) {
                    $clientData['isClient'] = 0;

                    return $clientData;
                }
            ],
            [
                function($data) {
                    return $data;
                },
                404,
                '',
                function($clientData, FixtureFactory $fixtureFactory) {
                    $clientData['activationCode'] = 'not_exists';

                    return $clientData;
                }
            ],
            [
                function($data) {
                    return $data;
                },
                404,
                '',
                function($clientData, FixtureFactory $fixtureFactory) {
                    // Doesn't fit this method
                    $clientData['activationCodeType'] = User::HASH_TYPE_FORGOT_PASSWORD;

                    return $clientData;
                }
            ],
            [
                function($data) {
                    return $data;
                },
                404,
                '',
                function($clientData, FixtureFactory $fixtureFactory) {
                    // Doesn't fit this method
                    $clientData['activationCodeType'] = User::HASH_TYPE_STUDENT_REGISTRATION_CONFIRMATION;

                    return $clientData;
                }
            ],
            [
                function($data) {
                    unset($data['username']);

                    return $data;
                },
                400,
                'username',
            ],
            [
                function($data) {
                    unset($data['password']);

                    return $data;
                },
                400,
                'password',
            ],
            [
                function($data) {
                    unset($data['confirm_password']);

                    return $data;
                },
                400,
                'password',
            ],
            [
                function($data) {
                    $data['confirm_password']=123;
                    $data['confirm_password']=456;

                    return $data;
                },
                400,
                'password',
            ],
            [
                function($data) {
                    unset($data['company_name']);

                    return $data;
                },
                400,
                'company_name',
            ],
            [
                function($data) {
                    unset($data['phone_number']);

                    return $data;
                },
                400,
                'phone_number',
            ],
            [
                function($data) {
                    unset($data['phone_number']);

                    return $data;
                },
                400,
                'phone_number',
            ],
        ];
    }
}
