<?php
namespace App\Validator\Constraint;

use App\Entity\User;
use Symfony\Component\Validator\Constraint;

class ClientPublicRegistration extends Constraint
{
    public $captchaWrongMessage = 'Captcha is not correct';

    /**
     * @var User
     */
    public $user;

    public $clientIp;

    public function getRequiredOptions()
    {
        return ['user', 'clientIp'];
    }
}
