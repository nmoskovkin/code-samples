<?php
namespace App\Validator\Constraint;

use App\Entity\User;
use App\Service\Mapper\JmsMappingContextFactory;
use App\Service\Mapper\PropertyMapper;
use App\Service\RecaptchaService;
use App\Service\ViolationService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ClientPublicRegistrationValidator extends ConstraintValidator
{
    private $recaptchaService;
    private $mappingContextFactory;
    private $propertyMapper;
    private $violationService;
    private $validator;
    
    public function __construct(
        RecaptchaService $recaptchaService,
        JmsMappingContextFactory $mappingContextFactory,
        PropertyMapper $propertyMapper,
        ViolationService $violationService,
        ValidatorInterface $validator
    ) {
        $this->recaptchaService = $recaptchaService;
        $this->mappingContextFactory = $mappingContextFactory;
        $this->propertyMapper = $propertyMapper;
        $this->violationService = $violationService;
        $this->validator = $validator;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint|ClientPublicRegistration $constraint The constraint for the validation
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function validate($value, Constraint $constraint)
    {
        $mappingContext = $this->mappingContextFactory->create(User::class, ['client.post.public_registration']);
        $this->propertyMapper->map($constraint->user, $value, $mappingContext);

        $violations = $this->validator->validate($constraint->user, null, ['client.post.public_registration']);
        if (count($violations)) {
            $this->violationService->addViolationsToContext('', $this->context, $violations);
        }

        if (!$constraint->user->getRecaptcha()
            || !$this->recaptchaService->verify($constraint->user->getRecaptcha(), $constraint->clientIp)
        ) {
            $this->context->buildViolation($constraint->captchaWrongMessage)->atPath('recaptcha')->addViolation();
        }
    }
}
