# Code Description

- ClientController - this class describes endpoints and document it using swagger (OpenApi). Also sometimes we use symfony controllers to specify user's role that is allowed to run this method
- ClientControllerTest - is an integration test. We use the library called FactoryGirl to set up data to database that are requred in test
then we call some REST methods and check the answer correctness.
- ClientPublicRegistration - is a constraint, we use it along with the symfony validator
- ClientPublicRegistrationValidator - tests request data to be correct according to a business logic, also using symfony validator.
- UserTransformer - is a transformer. This term belongs to the library called Fractal (https://packagist.org/packages/league/fractal)
we use this library to return json data in the JSON-API format. 
